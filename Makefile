bin/mostrarBytes: obj/arreglo.o obj/showBytes.o
	gcc obj/arreglo.o obj/showBytes.o -o bin/mostrarBytes

obj/arreglo.o:
	gcc -Wall -c -I include/ src/arreglo.c -o obj/arreglo.o

obj/showBytes.o: src/showBytes.c
	gcc -Wall -c src/showBytes.c -o obj/showBytes.o

.PHONY: clean
clean:
	rm bin/* obj/*
