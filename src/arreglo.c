#include <stdio.h>
#include "../include/arreglo.h"

typedef struct data{
    int a;
    char *b;
}tba;

int main(){
    int arr[10] = {};
    printf("Ingresar 10 números: \n");
    for(int i = 0; i < 10; i++){
        printf("Número %d: ", i+1);
        scanf("%d", &arr[i]);
        printf("\n");
    }

    for(int i = 0; i < 10; i++){
        showBytes(&arr[i], sizeof(int));
    }

    tba datos;
    datos.a = 211658512;
    datos.b = "Mensaje de prueba";
    printf("int: %d\nstring: %s\n",datos.a,datos.b);
    showBytes(&datos.a, sizeof(datos.a));
    showBytes(&datos.b, sizeof(datos.b));
    return 0;
}
