#include <stdio.h>
#include <stdlib.h>

void showBytes(void *valor, unsigned int tam){
    unsigned char *ptr_b = valor;
    printf("%p  ", valor);
    for(int i=0; i<tam; i++){
        printf("%02X ", *(ptr_b+i));
    }
    printf("\n");
}
